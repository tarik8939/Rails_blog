class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]


  def index

      #не відсортовані
      #@posts = Post.all
      # відсортовані
      @posts = Post.all.ordered.with_categories.paginate(page: params[:page], per_page: 6)

  end

  def show
    load_post
  end

  def new
    #load_categories
    @post = Post.new
  end

  def create
    # @post = Post.new(post_params)
    # @post.save
    # redirect_to @post

    @post = Post.new(post_params)
    if @post.save
      redirect_to @post, success: 'Post successfully created'
    else
      render 'new', danger: 'Post not created'
    end
  end

  def edit
    #load_categories
    load_post
  end

  def update
    load_post
    # @post.update(post_params)
    # redirect_to :post, success: 'Post successfully updated'
    if @post.update_attributes(post_params)
      redirect_to :post, success: 'Post successfully updated'
    else
      render 'edit', danger: 'Post not updated'
    end
  end

  def delete
    load_post
    @post.destroy
    redirect_to :posts, success: 'Post successfully deleted'
  end


  private

  def post_params
    params.require(:post).permit(:title, :text, :category_id)
  end

  def load_post
    @post = Post.find(params[:id])
  end

  # def load_categories
  #   @categories = Category.all
  # end


end
